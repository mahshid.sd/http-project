import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service'

import { Router } from '@angular/router';


@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {


  userid: any;
  user: any;
  data: any;
  constructor(private route: ActivatedRoute,private userService: UserService) { }

  ngOnInit(): void {
  this.getUser();
  console.log(this.user);
  }

  getuserid(){
    this.route.paramMap.subscribe(params => {
      this.userid = params.get('id');
    });
  }

  getUser(){
    this.getuserid();
    this.userService.getUser(this.userid)
    .subscribe(
      (data: any) => {
        this.data = { ...data.data }
        this.user = Object.values(this.data);
       },
    );
  }
  getu(){
    console.log(this.data);
  }
}
