import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserListComponent } from './user-list/user-list.component';
import { UserPageComponent } from './user-page/user-page.component'
  import { from } from 'rxjs';

const routes: Routes = [
  {path: 'user/:id', component: UserPageComponent},
  {path: '', component: UserListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
