import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  getUserList () {
    return this.http.get('https://reqres.in/api/users?page=1')
  }

  getUser(id: any){
    return this.http.get(`https://reqres.in/api/users/${id}`)
  }
}
