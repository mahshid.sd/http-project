import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service'

import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.getUser();
  }
  data: any;
  users: any[];
  error: any;

  loguser(){
    console.log(this.users);
  }
  getUser(){
    this.userService.getUserList()
    .subscribe(
      (data: any) => {
        this.data = { ...data.data }
        this.users = Object.values(this.data);
       },
      error => this.error = error
    );
  }

  onUserClick(user: any) {
    this.router.navigate(['/user', user.id])
  }
}
